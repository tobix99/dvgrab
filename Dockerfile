# build dvgrab from source with gcc image
FROM gcc AS builder
WORKDIR /repo/
ADD ./source /repo/dvgrab
RUN apt-get update && apt-get install -y libraw1394-dev libavc1394-dev libiec61883-dev libdv4-dev libquicktime-dev && \
    cd dvgrab && autoreconf -vif && ./configure && make

# create image with dvgrab and the start script
FROM ubuntu

WORKDIR /usr/local/bin/
COPY --from=builder /repo/dvgrab/dvgrab .

WORKDIR /app/
ADD start.sh /app/start.sh

RUN chmod +x /app/start.sh && \
    apt-get update && apt-get install -y libraw1394-11 libavc1394-0 libiec61883-0 libjpeg8 libjpeg62 libdv4 libquicktime2 \
    libc6 libgcc1 libstdc++6

WORKDIR /media/

CMD ["/app/start.sh"]